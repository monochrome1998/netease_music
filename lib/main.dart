import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netease_music/constants.dart';
import 'package:netease_music/screens/discover_screen.dart';

void main() {
  runApp(const MyApp());
  if (Platform.isAndroid) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '网易云音乐',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        textTheme: const TextTheme(bodyText1: textStyle),
      ),
      home: const NetEaseMusic(),
    );
  }
}

class NetEaseMusic extends StatefulWidget {
  const NetEaseMusic({Key? key}) : super(key: key);

  @override
  _NetEaseMusicState createState() => _NetEaseMusicState();
}

class _NetEaseMusicState extends State<NetEaseMusic> {
  /// 3个底部菜单页
  final _screens = <Widget>[
    const NetEaseDiscover(),
    const SizedBox(child: Center(child: Text("敬请期待")),),
    const SizedBox(child: Center(child: Text("敬请期待")),),
  ];

  /// 当前选中的页面
  int _currentScreen = 0;

  /// 当点击底部导航时
  onBottomNavTap(index) {
    setState(() {
      _currentScreen = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: _screens.elementAt(_currentScreen),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentScreen,
        onTap: (index) => onBottomNavTap(index),
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.music_note_outlined),
            label: "发现",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.library_music_outlined),
            label: "我的",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people_alt_outlined),
            label: "关注",
          )
        ],
      ),
    );
  }
}
