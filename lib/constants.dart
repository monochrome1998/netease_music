import 'package:flutter/material.dart';

/// Colors
const primaryColor = Color.fromARGB(255, 235, 77, 68);
const textColor = Color.fromARGB(255, 55, 55, 56);
const textLightColor = Color.fromARGB(255, 113, 113, 113);

/// 渐变色
const linearStart = Color.fromARGB(255, 239, 242, 244);
const linearEnd = Colors.white;

const textStyle = TextStyle(
  fontSize: 18.0,
  fontWeight: FontWeight.w700,
);

const subTextStyle = TextStyle(
  fontSize: 12.0,
  fontWeight: FontWeight.w700,
);

/// 默认内边距
const defaultPadding = 20.0;
