/// 根据语言推荐的列表数据
class LanguageRecommend {
  /// 图片
  final String image;

  /// 歌名
  final String song;

  /// 歌手
  final String singer;

  /// 构造器
  LanguageRecommend(this.image, this.song, this.singer);

  @override
  String toString() => "image:$image, song:$song, singer:$singer";
}
