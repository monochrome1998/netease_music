import 'package:flutter/material.dart';

/// 发现页（首页）卡片选单数据
class DiscoverCardItemMap {
  /// 标题
  final String title;

  /// 图标
  final IconData icon;

  /// 构造器
  DiscoverCardItemMap(this.title, this.icon);
}
