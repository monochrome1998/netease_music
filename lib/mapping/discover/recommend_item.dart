/// 推荐歌单
class RecommendItem {
  /// 标题
  final String title;

  /// 图片
  final String image;

  /// 播放次数
  final String playTimes;

  /// 构造器
  RecommendItem(this.title, this.image, this.playTimes);
}
