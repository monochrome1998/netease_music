import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:netease_music/constants.dart';
import 'package:netease_music/mapping/discover/card_item.dart';
import 'package:netease_music/mapping/discover/language_recommend.dart';
import 'package:netease_music/mapping/discover/recommend_item.dart';
import 'package:netease_music/screens/play_screen.dart';

/// 发现页
class NetEaseDiscover extends StatefulWidget {
  const NetEaseDiscover({Key? key}) : super(key: key);

  @override
  _NetEaseDiscoverState createState() => _NetEaseDiscoverState();
}

class _NetEaseDiscoverState extends State<NetEaseDiscover> {
  // 搜索提示词
  final _searchHintText = "一遍让你上瘾的韩语流行";

  // 轮播图图片
  final _swiperData = <String>[
    "assets/images/discover_pic.png",
    "assets/images/discover_pic1.png",
    "assets/images/discover_pic2.png",
    "assets/images/discover_pic3.png",
    "assets/images/discover_pic4.png",
    "assets/images/discover_pic5.png",
    "assets/images/discover_pic6.png",
    "assets/images/discover_pic7.png",
    "assets/images/discover_pic8.png",
    "assets/images/discover_pic9.png",
    "assets/images/discover_pic10.png",
    "assets/images/discover_pic11.png",
  ];

  // 卡片数据
  final _cardItemData = <DiscoverCardItemMap>[
    DiscoverCardItemMap("每日推荐", Icons.today_outlined),
    DiscoverCardItemMap("私人FM", Icons.radio_outlined),
    DiscoverCardItemMap("歌单", Icons.queue_music_outlined),
    DiscoverCardItemMap("排行榜", Icons.trending_up_outlined),
    DiscoverCardItemMap("数字专辑", Icons.album_outlined),
    DiscoverCardItemMap("专注冥想", Icons.psychology_outlined),
  ];

  // 推荐歌单数据
  final _recommendData = <RecommendItem>[
    RecommendItem(
        "耳朵福利：百首好听宝藏热歌精选", "assets/images/discover_card1.jpg", "92.1亿"),
    RecommendItem("健身跑步BGM", "assets/images/discover_card2.jpg", "429万"),
    RecommendItem("你总要一个人，经历些特别的日子", "assets/images/discover_card3.jpg", "∞"),
    RecommendItem(
        "欧美R&B轻快小众&流行浪漫舒缓cool电子", "assets/images/discover_card4.jpg", "69万"),
  ];

  // 根据语言推荐的歌曲数据
  final _languageRecommendData = <LanguageRecommend>[
    LanguageRecommend("assets/images/sheet1.jpg", "Celebrity", "IU"),
    LanguageRecommend("assets/images/sheet2.jpg", "听见下雨的声音", "周杰伦"),
    LanguageRecommend("assets/images/sheet3.jpg", "等你下课", "周杰伦"),
    LanguageRecommend("assets/images/sheet4.jpg", "一定会", "林俊杰"),
    LanguageRecommend("assets/images/sheet5.jpg", "群青", "YOASOBI"),
  ];

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.grey.withOpacity(0.1),
          ),
          child: Column(
            children: [
              Expanded(
                // 尽可能占用所有区域，且可以滑动
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      // 顶部大连体卡片
                      buildTopBigCard(),
                      // 语言分类推荐
                      buildLanguageRecommend()
                    ],
                  ),
                ),
              ),
              // 底部固定播放器
              buildBottomFixedPlayer()
            ],
          ),
        ),
      ),
    );
  }

  Container buildTopBigCard() {
    return Container(
      padding: const EdgeInsets.only(
        left: defaultPadding,
        right: defaultPadding,
        bottom: defaultPadding,
        top: defaultPadding / 4,
      ),
      decoration: const BoxDecoration(
        // color: Colors.white,
        gradient: LinearGradient(
          colors: [linearStart, linearEnd],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(defaultPadding),
          bottomRight: Radius.circular(defaultPadding),
        ),
      ),
      child: Column(
        children: [
          Row(
            children: [
              // 左侧抽屉
              const Icon(Icons.menu_outlined),
              // 搜索框
              Expanded(
                child: Container(
                  padding: const EdgeInsets.all(defaultPadding / 2),
                  child: Container(
                    height: 40.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    child: TextField(
                      autofocus: false,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(
                          borderSide: BorderSide.none,
                        ),
                        hintText: _searchHintText,
                        contentPadding: const EdgeInsets.all(0),
                        fillColor: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              // 听歌识曲
              const Icon(Icons.mic_outlined),
            ],
          ),
          // 轮播卡片
          buildCarousel(),
          // 右滑选项，每日推荐等
          Container(
            width: double.infinity,
            height: 85.0,
            margin: const EdgeInsets.only(top: defaultPadding),
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: _cardItemData.length,
              itemBuilder: (context, index) => _sectionCard(
                context,
                _cardItemData.elementAt(index),
              ),
              separatorBuilder: (context, index) => const SizedBox(width: 40.0),
            ),
          ),
          const Divider(),
          // 推荐歌单bar
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("推荐歌单", style: textStyle),
              Row(
                children: const [
                  Text("更多"),
                  Icon(Icons.keyboard_arrow_right_outlined),
                ],
              ),
            ],
          ),
          const SizedBox(height: 20.0),
          // 推荐歌单卡片
          SizedBox(
            width: double.infinity,
            height: 180.0,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) => _recommendCard(
                _recommendData.elementAt(index),
              ),
              separatorBuilder: (context, index) => const SizedBox(width: 20.0),
              itemCount: _recommendData.length,
            ),
          ),
        ],
      ),
    );
  }

  Container buildCarousel() {
    return Container(
      width: 400.0,
      height: 155.0,
      margin: const EdgeInsets.only(top: defaultPadding),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Swiper(
          autoplay: true,
          duration: 500,
          itemCount: _swiperData.length,
          pagination: const SwiperPagination(),
          itemBuilder: (context, index) => Image.asset(
            _swiperData.elementAt(index),
            fit: BoxFit.cover,
            width: 400.0,
            height: 155.0,
          ),
        ),
      ),
    );
  }

  Container buildBottomFixedPlayer() {
    toPlayScreen() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const PlayScreen()),
      );
    }

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
      color: const Color.fromARGB(255, 250, 250, 250),
      width: double.infinity,
      height: 50.0,
      child: Row(
        children: [
          // 唱片转盘
          GestureDetector(
            onTap: () => toPlayScreen(),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50.0),
              child: Container(
                width: 50.0,
                height: 50.0,
                padding: const EdgeInsets.all(8.0),
                decoration: const BoxDecoration(color: Colors.black),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(45.0),
                  child: Image.asset(
                    "assets/images/discover_card2.jpg",
                    width: 45.0,
                    height: 45.0,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(width: 10.0),
          Expanded(
            child: GestureDetector(
              onTap: () => toPlayScreen(),
              child: Row(
                children: [
                  const Text("Blueming", style: textStyle),
                  Text(
                    " - IU",
                    style: subTextStyle.copyWith(color: Colors.grey),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: 40.0,
            height: 40.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey.withOpacity(0.5)),
              borderRadius: BorderRadius.circular(40.0),
            ),
            child: const Icon(Icons.play_arrow),
          ),
          const SizedBox(width: 20.0),
          const SizedBox(
            width: 40.0,
            height: 40.0,
            child: Icon(
              Icons.playlist_play_outlined,
              size: 40.0,
            ),
          ),
        ],
      ),
    );
  }

  Container buildLanguageRecommend() {
    return Container(
      margin: const EdgeInsets.only(
          top: defaultPadding / 2, bottom: defaultPadding / 2),
      padding: const EdgeInsets.symmetric(
        horizontal: defaultPadding,
        vertical: defaultPadding / 2,
      ),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Column(
        children: [
          // 顶部推荐词和按钮
          Row(
            children: [
              const Icon(
                Icons.refresh_outlined,
              ),
              const SizedBox(width: 5.0),
              const Expanded(
                  child: Text(
                "韩语 藏在耳机里的猫",
                style: textStyle,
              )),
              TextButton.icon(
                onPressed: () {},
                icon: const Icon(
                  Icons.play_arrow,
                ),
                label: const Text(
                  "播放",
                  style: textStyle,
                ),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.white),
                  shape: MaterialStateProperty.all(const StadiumBorder()),
                ),
              ),
            ],
          ),
          // 推荐内容，每个推荐都是一个横向的长条卡片
          SizedBox(
            height: 65.0 * 4,
            child: ListView.separated(
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    _languageRecommend(_languageRecommendData.elementAt(index))
                  ],
                );
              },
              separatorBuilder: (context, index) {
                return const SizedBox(height: 10.0);
              },
              itemCount: _languageRecommendData.length,
            ),
          )
        ],
      ),
    );
  }

  /// 根据语言分类的歌曲推荐卡片
  Row _languageRecommend(LanguageRecommend data) {
    return Row(children: [
      Stack(
        alignment: AlignmentDirectional.center,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: Image.asset(
              data.image,
              width: 55,
              height: 55,
              fit: BoxFit.cover,
            ),
          ),
          Positioned(
            child: Icon(
              Icons.play_arrow,
              color: Colors.white.withOpacity(0.7),
            ),
          )
        ],
      ),
      const SizedBox(width: 10.0),
      Expanded(
        child: Row(
          children: [
            Expanded(
              child: Text(
                data.song,
                style: textStyle,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Text(
              data.singer,
              style: subTextStyle.copyWith(
                color: textLightColor,
              ),
            )
          ],
        ),
      )
    ]);
  }

  /// 推荐歌单
  SizedBox _recommendCard(RecommendItem item) {
    return SizedBox(
      width: 125.0,
      child: Column(
        children: [
          Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Image.asset(
                  item.image,
                  width: 125.0,
                  height: 125.0,
                  fit: BoxFit.contain,
                ),
              ),
              Positioned(
                right: 5,
                top: 5,
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 203, 157, 116),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Row(
                    children: [
                      const Icon(
                        Icons.arrow_right_outlined,
                        color: Colors.white,
                        size: 20.0,
                      ),
                      Text(
                        item.playTimes,
                        style: subTextStyle.copyWith(
                          color: Colors.white,
                          fontSize: 12.0,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 10.0),
          Text(
            item.title,
            style: subTextStyle.copyWith(fontSize: 15.0),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          )
        ],
      ),
    );
  }

  /// 每日推荐、私人FM等选项卡片
  Widget _sectionCard(BuildContext context, DiscoverCardItemMap item) {
    return Column(
      children: [
        Container(
          child: Icon(
            item.icon,
            color: Theme.of(context).primaryColor,
          ),
          padding: const EdgeInsets.all(defaultPadding / 2),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor.withOpacity(0.1),
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),
        const SizedBox(height: defaultPadding / 2),
        Text(
          item.title,
          style: subTextStyle,
        ),
      ],
    );
  }
}
